Domain Outbound Links Check
===========================
`Get Outbound LINKS of you web site`

Dependencies
============
  * anemone
  * nokogiri
  * uri
  * open-uri
  * open_uri_redirections

Run Application
===============
`./outbound.rb {URL}`

Authors
===================

*Created by* Germán Alberto Giménez Silva [@gsgerman](https://twitter.com/gsgerman)

*E-mail:* ggerman@gmail.com

*Date:* 14-08-14